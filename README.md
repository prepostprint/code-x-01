# CodeX - 01: PrePostPrint

"Code X" est un journal publié à l'occasion du salon de l’édition alternative organisé par PrePostPrint à La Gaîté Lyrique, le 21 octobre 2017.
Le journal a été réalisé en HTML et CSS et le PDF a été généré depuis la boîte d’impression de Google Chrome.

Pour acheter le journal sur le site de l’éditeur : [http://www.editions-hyx.com/fr/code-x](http://www.editions-hyx.com/fr/code-x)



## Notes

Le PDF journal doit être généré depuis Chrome car c’est (pour le moment) le seul navigateur qui prend en compte les dimensions de page déclarées en CSS.

Pour fragmenter le contenu afin qu’il *coule* dans les différentes pages, j’ai utilisé les principes du module [CSS regions](https://www.w3.org/TR/css-regions-1/). Ces spécifications CSS ne sont pas implémentées dans les navigateurs, j’ai donc ajouté un [polyfill javascript](https://github.com/FremyCompany/css-regions-polyfill) développé par la Fremy Compagny.

Le projet nécessite d’être sur serveur pour que le polyfill css-region fonctionne. Vous pouvez l’ouvrir avec un serveur local, avec node js par exemple (voir guide d'utilisation).

Le layout des pages est construit avec CSS grid par endroits.



## Guide d’utilisation

Dans la console, allez dans le dossier racine du projet puis tapez : 

`npm install`  
`node serveur.js`

Ensuite, dans le navigateur Google Chrome: [http://localhost:1454/](http://localhost:1454/)   
Imprimez !

(Si cela ne marche pas, vérifiez que vous avez installé npm)



## Améliorations possibles

* travailler la ligne de base et les line-height : trouver le juste calcul entre le line-height, l’endroit exact où le polyfill coupe et le module d’impression de Chrome qui modifie la balance des pages/colonnes. C’est un point très difficile que je n’ai pas eu le temps de travailler.
* utilisation des counters CSS pour les notes
* les breaks ne marchent pas dans le polyfill, j’ai dû tricher en ajoutant des marges à certains endroits


## Colophon

**Graphisme**  
Julie Blanc, Quentin Juhel

**Typographies**   
Nanook, par Lucas Le Bihan
Sinkin Sans, par K-Type
Panamera, par Bastien Sozeau
HK Grotesk, par Hanken Design Co.

**Illustration de couverture**
Raphaël Bastide
 © Creative commons CC-BY-SA

© Creative commons « attribution » – Pas d'utilisation commerciale – CC BY-NC-SA 4.0

Éditions HYX : Olivier Buslot, Emmanuel Cyriaque,  Marc-Antoine Perdereau 1, rue du Taureau, F-45000 Orléans   
contact@editions-hyx.com / editions-hyx.com

260 × 360 mm, 16 pages

---

Pour suivre mon travail : [julie-blanc.fr](http://julie-blanc.fr/) - Twitter : [@Julie_McFly](https://twitter.com/Julie_McFly)

